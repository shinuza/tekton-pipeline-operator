package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	pipelinev1beta1 "github.com/tektoncd/pipeline/pkg/apis/pipeline/v1beta1"
	"github.com/tektoncd/pipeline/pkg/client/clientset/versioned/scheme"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/tools/clientcmd"

	"k8s.io/client-go/rest"
)

const GroupName = "tekton.dev"
const GroupVersion = "v1beta1"

var kubeconfig string

func init() {
	flag.StringVar(&kubeconfig, "kubeconfig", "", "path to Kubernetes config file")
	flag.Parse()
}

func main() {
	var config *rest.Config
	var err error

	kubeconfig := "/home/deployer/.kube/config"

	if kubeconfig == "" {
		fmt.Printf("using in-cluster configuration")
		config, err = rest.InClusterConfig()
	} else {
		fmt.Printf("using configuration from '%s'", kubeconfig)
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
	}

	if err != nil {
		log.Fatalf("Unable to create in-cluster config: %v", err)
	}

	groupVersion := schema.GroupVersion{Group: GroupName, Version: GroupVersion}

	pipelinev1beta1.AddToScheme(scheme.Scheme)
	metav1.AddToGroupVersion(scheme.Scheme, groupVersion)

	config.ContentConfig.GroupVersion = &groupVersion
	config.APIPath = "/apis"
	config.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	restClient, err := rest.RESTClientFor(config)
	if err != nil {
		panic(err)
	}
	result := &pipelinev1beta1.Pipeline{}
	opts := metav1.GetOptions{}
	ctx := context.TODO()

	err = restClient.
		Get().
		Namespace("default").
		Resource("pipelines").
		Name("simple-pipeline").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do(ctx).
		Into(result)

	fmt.Println(result, err)
}
